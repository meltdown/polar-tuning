{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  unstable = import <unstable> {};
  inherit (lib) optional optionals;
  elixir = unstable.beam.packages.erlangR21.elixir_1_8;
in

mkShell {
  buildInputs = [ elixir git fwup squashfsTools file nodejs inotify-tools ]
    ++ optional stdenv.isDarwin coreutils-prefixed # For Nerves on macOS.
    ++ optional stdenv.isLinux x11_ssh_askpass; # For Nerves on Linux.

  # This hook is needed on Linux to make Nerves use the correct ssh_askpass.
  shellHooks = optional stdenv.isLinux ''
    export SUDO_ASKPASS=${x11_ssh_askpass}/libexec/x11-ssh-askpass
  '';
}

