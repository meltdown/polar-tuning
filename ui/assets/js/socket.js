import {Socket} from "phoenix"

let socket = new Socket("/socket", {params: {}})

socket.connect()

let channel = socket.channel("dials:lobby", {})
let buttons = document.querySelectorAll(".js-control")

buttons.forEach(function(input) {
  input.addEventListener("click", event => {
    let index = event.target.parentElement.dataset.index
    let position = document.querySelector(`.dial-${index}`).dataset.position
    let resolution = document.querySelector(`.dial-${index}`).dataset.resolution

    if (event.target.value === "-") {
        position--

        if (position < 0) {
            position = resolution - 1
        }
    } else {
        position++
        position = position % resolution
    }

    channel.push("set_from_ui", {index: index, position: position})
  })
})

channel.on("new_state", payload => {
  let dial = document.querySelector(`.dial-${payload.index}`)
  dial.dataset.position = payload.position
  dial.innerText = payload.position

  dial.parentElement.querySelector("[data-solved]").dataset.solved = payload.this_solved;
  document.querySelector(".graph").innerText = payload.graph;
  document.querySelector(".solved").dataset.solved = payload.all_solved
})

channel.join()
  .receive("ok", resp => { console.log("Joined successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })

export default socket
