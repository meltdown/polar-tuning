defmodule Ui.State.Dials do
  alias Ui.State.Dial
  alias Ui.State.Dials
  use Bitwise, only_operators: true

  @enforce_keys [:list]
  defstruct [:list]

  def list_from_seed(seed, number, resolution) do
    list =
      # Convert the seed into an array of binary elements
      (for <<x :: 2 <- seed >>, do: x)
      # Chunk into `resolution` sized groups
      |> Stream.chunk_every(resolution)
      # Take enough groups for our number of dials
      |> Stream.take(number)
      # Turn it into a list of dials, with initial data set to the correct position
      |> Enum.map(&(%Dial{data: &1, current_position: position_from_seed(&1, resolution)}))
      # Shift the data to make the logic easier later
      |> Enum.map(&(%Dial{&1 | data: Dial.shifted_data(&1, &1.current_position)}))

    %Dials{list: list}
  end

  def set_in_list(dials, index, position) do
    # Try get the dial at this index
    case Enum.fetch(dials.list, index) do
      # Not found, so just return
      :error -> {:error, dials}
      # Found, so update to the requested position
      {:ok, dial} ->
        if position >= Enum.count(dial.data) do
          # The position doesn't exist on this dial :(
          {:error, dials}
        else
          # Set the new position and return all the things
          dial = %Dial{dial | current_position: position}
          dials = %Dials{dials | list: List.replace_at(dials.list, index, dial)}

          {:ok, dials, dial}
        end
    end
  end

  def are_solved?(dials) do
    Enum.all?(dials.list, &(Dial.is_solved?(&1)))
  end

  # This sets the initial position of the dial. The recursion ensures none of the dials start in a completed state.
  defp position_from_seed([], resolution), do: round(resolution / 2) # Nothing more to recurse, so just set something
  defp position_from_seed([_ | tail] = seed, resolution) do
    # Set based on the (arbitrary) modulo of the sum of the seed array and the resolution (minus one as zero-indexed)
    case rem Enum.sum(seed), resolution - 1 do
      0 -> position_from_seed(tail, resolution) # Zero returned, so try again by dropping one item from the seed index
      position -> position # Not zero, so return this position
    end
  end

  defimpl String.Chars, for: Dials do
    # Long-term this should be a polar coordinate graph, but for testing the idea something like a bar-chart is fine.
    def to_string(dials) do
      dials.list
      # Get the shifted data
      |> Stream.map(&(Dial.shifted_data(&1)))
      # Zip the shifted data with the actual data, so we can compare later
      |> Stream.zip(Enum.map(dials.list, &(&1.data)))
      # Map over the zipped arrays
      |> Stream.map(fn {shifted, correct} ->
          # In each array, map over the pairs of data, comparing each individual shifted position with the correct one
          Stream.zip(shifted, correct) |> Enum.map(fn {s, c} ->
            s ^^^ c # XOR the bits, so we return the correct answer iff the shift is correct, otherwise the shift
          end)
        end)
      # Zip again, as we now have arrays of bits
      |> Stream.zip()
      # Map the zipped tuples to lists, then get the max value in each - if one dial is wrong then it should override
      # the bits in the correct dials.
      |> Stream.map(&(Tuple.to_list(&1) |> Enum.max()))
      # Turn the final result into a graph(ish)
      |> Enum.reduce("", fn i, str ->
        str <> case i do
          0 -> "░"
          1 -> "▒"
          2 -> "▓"
          3 -> "█"
          _ -> " "
        end
      end)
    end
  end
end
