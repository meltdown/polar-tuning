defmodule Ui.State do
  use GenServer
  alias Ui.State.Dial
  alias Ui.State.Dials
  require Logger

  ## Client API

  @doc """
  Starts the registry.
  """
  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: State)
  end

  @doc """
  Gets the whole list of dials

  Returns `{:ok, dials}`, `:error` otherwise.
  """
  def lookup() do
    GenServer.call(State, {:lookup})
  end

  @doc """
  Sets the dial position
  """
  def set(dial, position) do
    GenServer.cast(State, {:set, dial, position})
  end

  ## Server Callbacks

  def init(:ok) do
    Logger.debug "Initialising state"
    seed = Enum.take_random('abcdefghijklmnopqrstuvwxyz1234567890', 24)
    {:ok, %{dials: Dials.list_from_seed("#{seed}", 3, 8)}}
  end

  def handle_call({:lookup}, _from, %{dials: dials} = state) do
    {:reply, {:ok, dials}, state}
  end

  def handle_cast({:set, index, position}, %{dials: dials} = state) do
    dials = case Dials.set_in_list(dials, index, position) do
      {:error, dials} -> dials
      {:ok, dials, dial} ->
        Phoenix.PubSub.broadcast Nerves.PubSub, "dials", {:refresh}
        UiWeb.Endpoint.broadcast!("dials:lobby", "new_state", %{
          index: index,
          position: position,
          graph: "#{dials}",
          this_solved: Dial.is_solved?(dial),
          all_solved: Dials.are_solved?(dials)
        })

        dials
    end


    {:noreply, %{state | dials: dials}}
  end
end
