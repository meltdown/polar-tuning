defmodule Ui.State.Dial do
  alias Ui.State.Dial
  use Bitwise, only_operators: true

  @enforce_keys [:data, :current_position]
  defstruct [:data, :current_position]

  def is_solved?(dial) do
    # The dial is solved if the data at its current position matches the original data
    dial.data == Dial.shifted_data(dial)
  end

  # This shifts the dial data around, matching its current position.
  #
  # Effectively what that means is that if data is [1, 2, 3, 4] and the current position was `2`, the shifted data would
  # be [3, 4, 1, 2]. A shifted position of `1` would be [2, 3, 4, 1].
  def shifted_data(dial, override \\ nil)
  def shifted_data(%Dial{current_position: 0} = dial, _), do: dial.data # Don't try shift zero
  def shifted_data(dial, 0), do: dial.data # Don't try shift zero
  def shifted_data(%{current_position: current_position, data: data}, override) do
    # If we have an override use it, otherwise use the current position
    position = override || current_position

    # Slice the array at the position and then concat the two bits
    data
    |> Enum.slice(position..Enum.count(data))
    |> Enum.concat(Enum.slice(data, 0..position - 1))
  end
end
