defmodule UiWeb.DialsChannel do
  use Phoenix.Channel

  def join("dials:lobby", _message, socket) do
    {:ok, socket}
  end

  def join("dials:" <> _private_room_id, _params, _socket) do
    {:error, %{reason: "unauthorized"}}
  end

  def handle_in("set_from_ui", %{"index" => index, "position" => position}, socket) do
    Ui.State.set(String.to_integer(index), position)
    {:noreply, socket}
  end
end
