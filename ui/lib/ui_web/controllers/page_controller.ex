defmodule UiWeb.PageController do
  use UiWeb, :controller

  def index(conn, _params) do
    {:ok, dials} = Ui.State.lookup()
    render conn, "index.html", dials: dials
  end
end
