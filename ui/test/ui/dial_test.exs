defmodule Ui.State.DialTest do
  use ExUnit.Case
  alias Ui.State.Dial

  test "shifting data" do
    dial = %Dial{data: [1, 3, 1, 0, 1, 2, 1, 1], current_position: 4}
    assert [1, 2, 1, 1, 1, 3, 1, 0] == Dial.shifted_data(dial)
    assert [1, 3, 1, 0, 1, 2, 1, 1] == Dial.shifted_data(dial, 0)
    assert [1, 1, 1, 3, 1, 0, 1, 2] == Dial.shifted_data(dial, 6)
  end

  test "is solved" do
    dial = %Dial{data: [1, 3, 1, 0, 1, 2, 1, 1], current_position: 4}
    refute Dial.is_solved?(dial)

    dial = %Dial{dial | current_position: 0}
    assert Dial.is_solved?(dial)
  end
end
