defmodule Ui.State.DialsTest do
  use ExUnit.Case
  alias Ui.State.Dial
  alias Ui.State.Dials

  test "single dial from seed" do
    seed = "testseed"
    list = Dials.list_from_seed(seed, 1, 8)

    assert %Dials{list: [%Dial{data: [0, 1, 2, 1, 1, 1, 3, 1], current_position: 3}]} == list
  end

  test "two dials from seed" do
    seed = "tstseedmorewat"
    list = Dials.list_from_seed(seed, 2, 7)

    comparator = %Dials{list: [
      %Dial{data: [0, 1, 3, 0, 1, 3, 1], current_position: 3},
      %Dial{data: [1, 0, 1, 3, 3, 1, 3], current_position: 3}
    ]}

    assert comparator == list
  end

  test "setting in list" do
    seed = "tstseedwat"
    list = Dials.list_from_seed(seed, 3, 3)

    {:ok, list, _} = Dials.set_in_list(list, 0, 2)
    {:ok, list, _} = Dials.set_in_list(list, 2, 1)

    assert {:error, ^list} = Dials.set_in_list(list, 3, 1)
    assert {:error, ^list} = Dials.set_in_list(list, 1, 3)

    comparator = %Dials{list: [
      %Dial{data: [3, 1, 1], current_position: 2},
      %Dial{data: [1, 3, 0], current_position: 1},
      %Dial{data: [3, 1, 0], current_position: 1}
    ]}

    assert comparator == list
  end

  test "checking if solved" do
    seed = "tstseedwat"
    list = Dials.list_from_seed(seed, 3, 3)

    refute Dials.are_solved?(list)

    {:ok, list, _} = Dials.set_in_list(list, 0, 2)
    {:ok, list, _} = Dials.set_in_list(list, 1, 0)
    {:ok, list, _} = Dials.set_in_list(list, 2, 1)

    refute Dials.are_solved?(list)

    {:ok, list, _} = Dials.set_in_list(list, 0, 0)
    {:ok, list, _} = Dials.set_in_list(list, 2, 0)

    assert Dials.are_solved?(list)
  end
end
